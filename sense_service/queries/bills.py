from pydantic import BaseModel
from queries.pool import pool
from datetime import date
from typing import List, Optional

class BillsIn(BaseModel):
    user_id: int
    name: str
    date: date
    amount: int

class BillsOut(BaseModel):
    id: int
    user_id: int
    name: str
    date: date
    amount: int

class BillsQueries:
    def create_bill(self, bills: BillsIn, user_id: int) -> BillsOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT into bills
                            (user_id, name, date, amount)
                        VALUES
                            (%s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            user_id,
                            bills.name,
                            bills.date,
                            bills.amount
                        ],
                    )
                    id = result.fetchone()[0]
                    old_data = bills.dict()
                    return BillsOut(id=id, **old_data)
        except Exception as e:
            return {"message:" "Could not create bill" + str(e)}

    def get_bills(self, bill: BillsIn, user_id: int) -> List[BillsOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT *
                        FROM bills
                        WHERE bills.user_id = %s
                        """,
                        [user_id],
                    )
                    bills = []
                    for record in db:
                        bill = BillsOut(
                            id = record[0],
                            user_id = record[1],
                            name = record[2],
                            date = record[3],
                            amount = record[4],
                        )
                        bills.append(bill)
                    return bills
        except Exception:
            return {"message:" "Could not retrieve bills"}

    def get_one_bill(self, user_id: int, bill_id: int) -> BillsOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id, user_id, name, date, amount
                        FROM bills
                        WHERE id = %s
                        AND user_id = %s;
                        """,
                        [
                            bill_id,
                            user_id,
                        ],
                    )
                    record = db.fetchone()
                    if record is None:
                        return None
                    return BillsOut(
                        id = record[0],
                        user_id = record[1],
                        name = record[2],
                        date = record[3],
                        amount = record[4],
                    )
        except Exception as e:
            return {"message": "Could not retrieve the bill"}

    def update_bills(self, user_id: int, bill_id: int, bill: BillsOut) -> BillsOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE bills
                            set name = %s,
                                date = %s,
                                amount = %s
                        WHERE id = %s and user_id = %s
                        """,
                        [
                            bill.name,
                            bill.date,
                            bill.amount,
                            user_id,
                            bill_id
                        ],
                    )
                    old_data = bill.dict()
                    old_data["id"] = bill_id
                    old_data["user_id"] = user_id
                    return BillsOut(**old_data)
        except Exception:
            return {"message:" "Could not update bill"}

    def delete_bill(self, id: int, user_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE from bills
                        WHERE id = %s AND user_id = %s
                        """,
                        [id, user_id],
                    )
                    return True
        except Exception:
            return False
