from pydantic import BaseModel
from queries.pool import pool
from datetime import date
from enum import Enum
from typing import List

class CategoryEnum(str, Enum):
    groceries = "groceries"
    housing = "housing"
    misc = "misc"
    savings = "savings"
    subscriptions = "subscriptions"
    transportation = "transportation"
    travel = "travel"

class TransactionsIn(BaseModel):
    user_id: int
    date: date
    description: str
    category: CategoryEnum
    amount: int

class TransactionOut(BaseModel):
    id: int
    user_id: int
    date: date
    description: str
    category: CategoryEnum
    amount: int


class TransactionQueries:
    def create_transaction(
        self, transactions: TransactionsIn, user_id: int
        ) -> List[TransactionOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO transactions
                            (user_id, date, description, category, amount)
                        VALUES
                            (%s, %s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            user_id,
                            transactions.date,
                            transactions.description,
                            transactions.category,
                            transactions.amount
                        ],
                    )
                    id = result.fetchone()[0]
                    old_data = transactions.dict()
                    return TransactionOut(id=id, **old_data)
        except Exception as e:
            return {"message": "Could not create transaction: " + str(e)}

    def get_transactions(self, transaction: TransactionsIn, user_id: int) -> List[TransactionOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT *
                        FROM transactions as t
                        WHERE t.user_id = %s
                        """,
                        [user_id],
                    )
                    transactions = []
                    for record in db:
                        transaction = TransactionOut(
                            id = record[0],
                            user_id = record[1],
                            date = record[2],
                            description = record[3],
                            category = record[4],
                            amount = record[5]
                        )
                        transactions.append(transaction)
                    return transactions
        except Exception:
            return {"message": "Could not get transactions"}

    def get_one_transaction(self, user_id: int, transaction_id: int) -> TransactionOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id, user_id, date, description, category, amount
                        FROM transactions
                        WHERE id = %s
                        AND user_id = %s;
                        """,
                        [
                            transaction_id,
                            user_id
                        ],
                    )
                    record = db.fetchone()
                    if record is None:
                        return None
                    return TransactionOut(
                        id = record[0],
                        user_id = record[1],
                        date = record[2],
                        description = record[3],
                        category = record[4],
                        amount = record[5],
                    )
        except Exception as e:
            return {"message": "Could not get the transaction" + str(e)}

    def update_transaction(self, user_id: int, transaction_id: int, transaction: TransactionOut) -> TransactionOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE transactions
                            set date = %s,
                                description = %s,
                                category = %s,
                                amount = %s
                        WHERE id = %s and user_id = %s
                        """,
                        [
                            transaction.date,
                            transaction.description,
                            transaction.category,
                            transaction.amount,
                            user_id,
                            transaction_id
                        ],
                    )
                    old_data = transaction.dict()
                    old_data["id"] = transaction_id
                    old_data["user_id"] = user_id
                    return TransactionOut(**old_data)
        except Exception as e:
            return {"message": "Could not update transaction" + str(e)}

    def delete_transaction(self, id: int, user_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE from transactions
                        WHERE id = %s AND user_id = %s
                        """,
                        [id, user_id],
                    )
                    return True
        except Exception as e:
            return False
