steps = [
        [
        """
        CREATE TYPE category_name AS ENUM (
            'groceries', 'housing', 'travel', 'transportation', 'restaurants', 'bills',
            'subscriptions', 'savings', 'misc'
        );
        """,

        """
        DROP TYPE category_name
        """,
    ],
    [
        """
        CREATE TABLE transactions (
            id SERIAL PRIMARY KEY NOT NULL,
            user_id INT NOT NULL,
            date DATE NOT NULL,
            description VARCHAR(75),
            category category_name,
            amount INT NOT NULL
        );
        """,
        """
        DROP TABLE transactions
        """,
    ]
]
