steps = [
    [
        """
        CREATE TABLE bills (
            id SERIAL PRIMARY KEY NOT NULL,
            user_id INT NOT NULL,
            name VARCHAR(75),
            date DATE NOT NULL,
            amount INT NOT NULL
        );
        """,
        """
        DROP TABLE bills
        """,
    ]
]
