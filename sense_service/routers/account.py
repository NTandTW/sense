from fastapi import (
    Depends,
    Response,
    APIRouter,
    Request,
    HTTPException,
    status,
)
from jwtdown_fastapi.authentication import Token
from authenticator import authenticator

from pydantic import BaseModel
from queries.account import (
    AccountsIn,
    AccountsOut,
    AccountsQueries,
    DuplicateUsersError,
)

class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    account: AccountsOut

class HttpError(BaseModel):
    detail: str

router = APIRouter()

@router.get("/token", response_model=AccountToken | None)
async def get_token(
    request: Request,
    account: AccountsOut = Depends(authenticator.try_get_current_account_data),
) -> AccountToken | None:
    if authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "account": account,
        }


@router.post("/api/user", response_model=AccountToken | HttpError)
async def create_user(
    info: AccountsIn,
    request: Request,
    response: Response,
    accounts: AccountsQueries = Depends(),
):
    hashed_password = authenticator.hash_password(info.password)
    try:
        account = accounts.create(info, hashed_password)
    except DuplicateUsersError:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Unable to create account"
        )
    form = AccountForm(username=info.username, password=info.password)
    token = await authenticator.login(response, request, form, accounts)
    return AccountToken(account=account, **token.dict())

@router.delete("/api/{id}", response_model=bool)
def delete_account(
    id: int,
    response: Response,
    repo: AccountsQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> bool:
    if account_data is not None:
        return repo.delete(id)
    else:
        raise HTTPException(status_code=401, detail="Could not delete account")
