from fastapi import APIRouter, Depends
from queries.bills import BillsIn, BillsOut, BillsQueries
from authenticator import authenticator
from typing import List

router = APIRouter()

@router.post("/api/bills", response_model=BillsOut)
def create_bill(
    info: BillsIn,
    repo: BillsQueries = Depends(),
    account_data = Depends(authenticator.get_current_account_data)
):
    user_id = account_data["id"]
    return repo.create_bill(info, user_id)

@router.get("/api/bills", response_model=List[BillsOut])
def get_bills(
    repo: BillsQueries = Depends(),
    account_data = Depends(authenticator.get_current_account_data),
):
    user_id = account_data["id"]
    return repo.get_bills(repo, user_id)

@router.get("/api/bills/{id}", response_model=BillsOut)
def get_one_bill(
    id: int,
    repo: BillsQueries = Depends(),
    account_data = Depends(authenticator.get_current_account_data)
):
    user_id = account_data["id"]
    return repo.get_one_bill(user_id, id)

@router.put("/api/bills/{id}", response_model=BillsOut)
def update_bill(
    id: int,
    bill: BillsOut,
    repo: BillsQueries = Depends(),
    account_data = Depends(authenticator.get_current_account_data)
):
    user_id = account_data["id"]
    return repo.update_bills(user_id, id, bill)

@router.delete("/api/bills/{id}", response_model=bool)
def delete_bill(
    id: int,
    repo: BillsQueries = Depends(),
    account_data = Depends(authenticator.get_current_account_data)
):
    user_id = account_data["id"]
    return repo.delete_bill(id, user_id)
