from fastapi import APIRouter, Depends
from queries.transactions import (
    TransactionsIn, TransactionOut, TransactionQueries
)
from authenticator import authenticator
from typing import List

router = APIRouter()

@router.post("/api/transactions", response_model=TransactionOut)
def create_transaction(
    info: TransactionsIn,
    repo: TransactionQueries = Depends(),
    account_data = Depends(authenticator.get_current_account_data),
):
    user_id = account_data["id"]
    return repo.create_transaction(info, user_id)

@router.get("/api/transactions", response_model=List[TransactionOut])
def get_transactions(
    repo: TransactionQueries = Depends(),
    account_data = Depends(authenticator.get_current_account_data),
):
    user_id = account_data["id"]
    return repo.get_transactions(repo, user_id)

@router.get("/api/transactions/{id}", response_model=TransactionOut)
def get_one_transaction(
    id: int,
    repo: TransactionQueries = Depends(),
    account_data = Depends(authenticator.get_current_account_data),
):
    user_id = account_data["id"]
    return repo.get_one_transaction(user_id, id)

@router.put("/api/transactions/{id}", response_model=TransactionOut)
def update_transaction(
    id: int,
    transaction: TransactionsIn,
    repo: TransactionQueries = Depends(),
    account_data = Depends(authenticator.get_current_account_data),
):
    user_id = account_data["id"]
    return repo.update_transaction(user_id, id, transaction)

@router.delete("/api/transactions/{id}", response_model = bool)
def delete_transaction(
    id: int,
    repo: TransactionQueries = Depends(),
    account_data = Depends(authenticator.get_current_account_data)
):
    user_id = account_data["id"]
    return repo.delete_transaction(id, user_id)
