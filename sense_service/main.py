from fastapi import FastAPI
from authenticator import authenticator
from routers import account, transactions, bills
import os
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

origins = [
    os.environ.get("CORS_HOST", None),
    "http://localhost:3000",
]


app.include_router(account.router)
app.include_router(transactions.router)
app.include_router(bills.router)
app.include_router(authenticator.router)


app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
