"use client";
import "./globals.css";
import { Inter } from "next/font/google";
import { AuthProvider } from "@galvanize-inc/jwtdown-for-react";
import Nav from "../components/nav.js";
import React from "react";

const inter = Inter({ subsets: ["latin"] });

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <AuthProvider baseUrl={"http://localhost:8000"}>
          <Nav />
          {children}
        </AuthProvider>
      </body>
    </html>
  );
}
