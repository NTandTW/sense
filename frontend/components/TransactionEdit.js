import React, { useState } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";

export default function TransactionEdit({ onClose, id }) {
  const { token } = useToken();
  const [date, setDate] = useState("");
  const [description, setDescription] = useState("");
  const [category, setCategory] = useState("");
  const [amount, setAmount] = useState("");

  const handleDateChange = (e) => {
    setDate(e.target.value);
  };

  const handleDescriptionChange = (e) => {
    setDescription(e.target.value);
  };

  const handleCategoryChange = (e) => {
    setCategory(e.target.value);
  };

  const handleAmountChange = (e) => {
    setAmount(e.target.value);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const transactionData = {};

    const transactionsURL = `http://localhost:8000/api/transactions/${id}`;
    const fetchConfig = {
      method: "PUT",
      body: JSON.stringify(transactionData),
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(transactionsURL, fetchConfig);

    if (response.ok) {
      await response.json();
      setDate("");
      setDescription("");
      setCategory("");
      setAmount("");
      onClose(); // Close modal after successful edit
    } else {
      console.error("Could not edit transaction");
    }
  };

  return (
    <div className="max-w-4xl mx-auto mt-8">
      <div className="modal-backdrop-popup">
        <div className="modal-content-popup shadow-md sm:rounded-lg bg-white dark:bg-gray-800">
          <h1 className="text-2xl font-bold mb-4">Edit Transaction</h1>
          <form onSubmit={handleSubmit} id="transaction-edit-form">
            <div className="flex flex-wrap mb-4">
              <input
                type="date"
                value={date}
                onChange={handleDateChange}
                required
                className="input-field mr-4 mb-2 text-black"
              />
              <input
                type="text"
                value={description}
                onChange={handleDescriptionChange}
                placeholder="Description"
                required
                className="input-field mr-4 mb-2 text-black"
              />
              <input
                type="text"
                value={category}
                onChange={handleCategoryChange}
                placeholder="Category"
                required
                className="input-field mr-4 mb-2 text-black"
              />
              <input
                type="number"
                value={amount}
                onChange={handleAmountChange}
                placeholder="Amount"
                required
                className="input-field mb-2 text-black"
              />
            </div>
            <div className="mt-4">
              <button
                type="submit"
                className="px-4 py-2 bg-blue-500 text-white rounded-md hover:bg-blue-600 focus:outline-none focus:bg-blue-600"
              >
                Update
              </button>
              <button
                onClick={onClose}
                className="ml-2 px-4 py-2 bg-red-500 text-white rounded-md hover:bg-red-600 focus:outline-none focus:bg-red-600"
              >
                Cancel
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
