"use client";
import { useState, useEffect } from "react";
import Navbar from "./Navbar.js";
// import CollapsedNavbar from "./CollapsedNavbar.js";

export default function Nav() {
  const [width, setWidth] = useState(0);
  const breakpoint = 620;

  //windows.innerWidth making reference error
  useEffect(() => {
    const handleWindowResize = () => setWidth(window.innerWidth);
    window.addEventListener("resize", handleWindowResize);

    return () => window.removeEventListener("resize", handleWindowResize);
  }, []);

  return <Navbar />;
  // return width < breakpoint ? <CollapsedNavbar /> : <Navbar />;
}
