"use client";
import React, { useEffect, useState } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import TransactionEdit from "./TransactionEdit";
import { DotsVerticalIcon } from "@heroicons/react/outline";
import "./editModal.css";

export default function TransactionList() {
  const { token } = useToken();

  const [transactions, setTransactions] = useState([]);
  const [showEditForm, setShowEditForm] = useState(false);
  const [editTransactionId, setEditTransactionId] = useState(null);
  const [openOptionsId, setOpenOptionsId] = useState(null);

  useEffect(() => {
    const fetchTransactions = async () => {
      if (token) {
        const transactionsURL = `http://localhost:8000/api/transactions`;
        const fetchConfig = {
          method: "GET",
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
          },
        };
        const response = await fetch(transactionsURL, fetchConfig);
        if (response.ok) {
          const data = await response.json();
          setTransactions(data);
        }
      }
    };
    fetchTransactions();
  }, [token, showEditForm]);

  const handleEdit = (id) => {
    setEditTransactionId(id);
    setShowEditForm(true);
  };

  const handleCloseEditForm = () => {
    setShowEditForm(false);
    setEditTransactionId(null);
  };

  const handleMoreOptionsClick = (id) => {
    setOpenOptionsId(openOptionsId === id ? null : id);
  };

  const handleDelete = async (id) => {
    const transactionsURL = `http://localhost:8000/api/transactions/${id}`;
    const fetchConfig = {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    };
    const response = await fetch(transactionsURL, fetchConfig);
    if (response.ok) {
      setTransactions(
        transactions.filter((transaction) => transaction.id !== id)
      );
    } else {
      console.error("Could not delete transaction.");
    }
  };

  return (
    <>
      <div className="max-w-4xl mx-auto mt-8">
        <div className="flex flex-col">
          <div className="overflow-x-auto shadow-md sm:rounded-lg">
            <div className="inline-block min-w-full align-middle">
              <div className="overflow-hidden">
                <table className="min-w-full divide-y divide-gray-200 table-fixed dark:divide-gray-700">
                  <thead className="bg-gray-100 dark:bg-gray-700">
                    <tr>
                      <th scope="col" className="p-4"></th>
                      <th
                        scope="col"
                        className="py-3 px-6 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400"
                      >
                        Date
                      </th>
                      <th
                        scope="col"
                        className="py-3 px-6 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400"
                      >
                        Description
                      </th>
                      <th
                        scope="col"
                        className="py-3 px-6 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400"
                      >
                        Category
                      </th>
                      <th
                        scope="col"
                        className="py-3 px-6 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400"
                      >
                        Amount
                      </th>
                      <th scope="col" className="p-4"></th>
                    </tr>
                  </thead>
                  <tbody className="bg-white divide-y divide-gray-200 dark:bg-gray-800 dark:divide-gray-700">
                    {transactions.map((transaction, index) => (
                      <tr
                        key={index}
                        className="hover:bg-gray-100 dark:hover:bg-gray-700 relative"
                      >
                        <td className="p-4 w-4"></td>
                        <td className="py-4 px-6 text-sm font-medium text-gray-900 whitespace-nowrap dark:text-white">
                          {transaction.date}
                        </td>
                        <td className="py-4 px-6 text-sm font-medium text-gray-500 whitespace-nowrap dark:text-white">
                          {transaction.description}
                        </td>
                        <td className="py-4 px-6 text-sm font-medium text-gray-500 whitespace-nowrap dark:text-white">
                          {transaction.category}
                        </td>
                        <td className="py-4 px-6 text-sm font-medium text-gray-900 whitespace-nowrap dark:text-white">
                          ${transaction.amount}
                        </td>
                        <td className="p-4 w-4 relative">
                          <button
                            onClick={() =>
                              handleMoreOptionsClick(transaction.id)
                            }
                            className="text-gray-400 hover:text-gray-800"
                          >
                            <DotsVerticalIcon className="h-5 w-5" />
                          </button>
                          {openOptionsId === transaction.id && (
                            <div className="absolute right-0 mt-2 w-48 bg-white rounded-md shadow-lg z-10">
                              <button
                                onClick={() => handleEdit(transaction.id)}
                                className="block w-full px-4 py-2 text-left text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900"
                              >
                                Edit
                              </button>
                              <button
                                onClick={() => handleDelete(transaction.id)}
                                className="block w-full px-4 py-2 text-left text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900"
                              >
                                Delete
                              </button>
                            </div>
                          )}
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

      {showEditForm && (
        <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50">
          <div className="bg-white dark:bg-gray-800 p-6 rounded-lg shadow-lg">
            <div className="modal-header">
              <h2>Edit Transaction</h2>
            </div>
            <div className="modal-body">
              <TransactionEdit
                token={token}
                onClose={handleCloseEditForm}
                id={editTransactionId}
              />
            </div>
            <div className="modal-footer">
              <button onClick={handleCloseEditForm}>Close</button>
            </div>
          </div>
        </div>
      )}
    </>
  );
}
